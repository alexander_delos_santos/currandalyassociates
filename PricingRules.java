import java.util.*;

public abstract class PricingRules {

	protected String promoCode;
	protected Set<Promo> promos;
	protected Promo appliedPromo;
	protected List<PriceModifier> priceModifiers = new ArrayList<>();
	protected List<ItemModifier> itemModifiers = new ArrayList<>();

	public PricingRules(Set<Promo> promos) {
		this.promos = promos;
	}

	public abstract void applyPromoCode(String promoCode);

	public abstract Double computeTotal(Map<SimProduct, Integer> cartItems);

	public abstract Map<SimProduct, Integer> getAllItems(Map<SimProduct, Integer> cartItems);
}