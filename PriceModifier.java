import java.util.*;
public interface PriceModifier {
	
	Double computeAdjustment(Map<SimProduct, Integer> cartItems);

}