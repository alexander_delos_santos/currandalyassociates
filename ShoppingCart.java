import java.util.*;

public class ShoppingCart {

	private Map<SimProduct, Integer> cartItems = new HashMap<>();
	private PricingRules pricingRules;

	public ShoppingCart(PricingRules pricingRules) {
		this.pricingRules = pricingRules;
	}


	public void add(SimProduct simProduct) {
		if (cartItems.get(simProduct) != null) {
			cartItems.put(simProduct, cartItems.get(simProduct) + 1);
		} else {
			cartItems.put(simProduct, 1);
		}
	}

	public void add(SimProduct simProduct, String promoCode) {
		add(simProduct);
		if (pricingRules != null) {
			pricingRules.applyPromoCode(promoCode);
		}
	}

	public Double total() {
		if (pricingRules == null) {
			return 0.00;
		}
		return pricingRules.computeTotal(cartItems);
	}

	public Map<SimProduct, Integer> items() {
		if (pricingRules == null) {
			return new HashMap<>();
		}
		return pricingRules.getAllItems(cartItems);
	}
}




