import java.util.*;

public class SmallBundlePriceModifier implements PriceModifier {

	public Double computeAdjustment(Map<SimProduct, Integer> cartItems) {
		Double total = 0.00;

		if (cartItems == null) {
			return total;
		}

		Set<SimProduct> keySet = cartItems.keySet();

		for (SimProduct sp : keySet) {
			Integer ultCount = cartItems.get(sp);
			if (sp.getCode().equals("ult_small")) {
				int totalCount = ultCount / 3;
				total += totalCount * sp.getPrice();
				break;
			}
		}

		return total * -1;
	}
	
}