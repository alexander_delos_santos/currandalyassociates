import java.util.*;
import java.text.DecimalFormat;

public class MainApplication {

	static SimProduct spSmall = new SimProduct("ult_small", "Unlimited 1GB", 24.90);
	static SimProduct spMedium = new SimProduct("ult_medium", "Unlimited 2GB", 29.90);
	static SimProduct spLarge = new SimProduct("ult_large", "Unlimited 5GB", 44.90, 3, 39.90);
	static SimProduct spGb = new SimProduct("1gb", "1 GB Data-pack", 9.90);
	static DecimalFormat df2 = new DecimalFormat(".##");

	public static void main(String[] args) {
		scenario1();		
		scenario2();		
		scenario3();		
		scenario4();		
	}

	public static void printItems(Map<SimProduct, Integer> items) {
		for (SimProduct sp : items.keySet()) {
			System.out.println("\t - " + items.get(sp) + "x " + sp.getName());
		}
	}

	public static void scenario1() {
		System.out.println("Scenario 1: 3x Unlimited 1GB, 1x Unlimited 5GB");

		PricingRules pricingRules = new DefaultPricingRules(getPromos());
		ShoppingCart cart = new ShoppingCart(pricingRules);

		cart.add(spSmall);
		cart.add(spSmall);
		cart.add(spSmall);
		cart.add(spLarge);

		System.out.println("Scenario 1 Total: $ " + df2.format(cart.total()));
		System.out.println("Scenario 1 Items:");
		printItems(cart.items());
		System.out.println();
	}

	public static void scenario2() {
		System.out.println("Scenario 2: 2x Unlimited 1GB, 4x Unlimited 5GB");

		PricingRules pricingRules = new DefaultPricingRules(getPromos());
		ShoppingCart cart = new ShoppingCart(pricingRules);

		cart.add(spSmall);
		cart.add(spSmall);
		cart.add(spLarge);
		cart.add(spLarge);
		cart.add(spLarge);
		cart.add(spLarge);

		System.out.println("Scenario 2 Total: $ " + df2.format(cart.total()));
		System.out.println("Scenario 2 Items:");
		printItems(cart.items());
		System.out.println();
	}

	public static void scenario3() {
		System.out.println("Scenario 3: 1x Unlimited 1GB, 2x Unlimited 2GB");

		PricingRules pricingRules = new DefaultPricingRules(getPromos());
		ShoppingCart cart = new ShoppingCart(pricingRules);

		cart.add(spSmall);
		cart.add(spMedium);
		cart.add(spMedium);

		System.out.println("Scenario 3 Total: $ " + df2.format(cart.total()));
		System.out.println("Scenario 3 Items:");
		printItems(cart.items());
		System.out.println();
	}

	public static void scenario4() {
		System.out.println("Scenario 4: 1x Unlimited 1GB, 1x GB Data-pack, 'I<3AMAYSIM' Promo Applied");

		PricingRules pricingRules = new DefaultPricingRules(getPromos());
		ShoppingCart cart = new ShoppingCart(pricingRules);

		cart.add(spSmall);
		cart.add(spGb, "I<3AMAYSIM");

		System.out.println("Scenario 4 Total: $ " + df2.format(cart.total()));
		System.out.println("Scenario 4 Items:");
		printItems(cart.items());
		System.out.println();
	}



	public static Set<Promo> getPromos() {
		Set<Promo> promos = new HashSet<>();
		Promo promo = new Promo();
		promo.setCode("I<3AMAYSIM");
		promo.setDiscount(0.1);
		promos.add(promo);
		return promos;
	}

}