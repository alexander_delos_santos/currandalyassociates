import java.util.*;

public class MediumItemModifier implements ItemModifier {

	public Map<SimProduct, Integer> computeAdjustment(Map<SimProduct, Integer> cartItems) {
		Map<SimProduct, Integer> items = new HashMap<>();

		if (cartItems == null) {
			return items;
		}

		Set<SimProduct> keySet = cartItems.keySet();

		for (SimProduct sp : keySet) {
			Integer ultCount = cartItems.get(sp);
			if (sp.getCode().equals("ult_medium")) {
				items.put(MainApplication.spGb, ultCount);
			}
		}

		return items;
	}
}