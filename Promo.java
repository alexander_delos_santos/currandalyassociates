import java.util.*;

public class Promo {

	private String code;
	private Double discount;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}


	public boolean equals(Object o)  {
		if (o == null || !(o instanceof Promo)) {
			return ((Promo)o).code.equals(code);
		}
		return o.equals(code);
	}

	public int hashCode() {
		return code.hashCode();
	}
}