import java.util.*;
public interface ItemModifier {
	
	Map<SimProduct, Integer> computeAdjustment(Map<SimProduct, Integer> cartItems);

}