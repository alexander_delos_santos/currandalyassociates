import java.util.*;

public class LargeBundlePriceModifier implements PriceModifier {

	public Double computeAdjustment(Map<SimProduct, Integer> cartItems) {
		Double total = 0.00;

		if (cartItems == null) {
			return total;
		}

		Set<SimProduct> keySet = cartItems.keySet();

		for (SimProduct sp : keySet) {
			Integer ultCount = cartItems.get(sp);
			if (sp.getCode().equals("ult_large")) {
				if (ultCount > sp.getMinBulkQty()) {
					total = (sp.getPrice() - sp.getBulkPrice()) * ultCount;
				}
				break;
			}
		}

		return total * -1;
	}
	
}