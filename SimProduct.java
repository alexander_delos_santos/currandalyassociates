public class SimProduct {

	private String code;
	private String name;
	private Double price;
	private Double bulkPrice;
	private Integer minBulkQty;

	public SimProduct() {

	}	

	public SimProduct(String code, String name, Double price) {
		this.code = code;
		this.name = name;
		this.price = price;
	}	

	public SimProduct(String code, String name, Double price, Integer minBulkQty,  Double bulkPrice) {
		this(code, name, price);
		this.bulkPrice = bulkPrice;
		this.minBulkQty = minBulkQty;
	}	

	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getBulkPrice() {
		return bulkPrice;
	}

	public void setBulkPrice(Double bulkPrice) {
		this.bulkPrice = bulkPrice;
	}

	public Integer getMinBulkQty() {
		return minBulkQty;
	}

	public void setMinBulkQty(Integer minBulkQty) {
		this.minBulkQty = minBulkQty;
	}

}