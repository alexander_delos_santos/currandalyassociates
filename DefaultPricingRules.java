import java.util.*;
import java.math.*;

public class DefaultPricingRules extends PricingRules {

	public DefaultPricingRules(Set<Promo> promos) {
		super(promos);

		priceModifiers.add(new SmallBundlePriceModifier());
		priceModifiers.add(new LargeBundlePriceModifier());

		itemModifiers.add(new MediumItemModifier());
	}

	public void applyPromoCode(String promoCode) {
		for (Promo p : promos) {
			if (p.getCode().equals(promoCode)) {
				appliedPromo = p;
				break;
			}
		}
	}

	public Double computeTotal(Map<SimProduct, Integer> cartItems) {
		Double total = 0.00;

		if (cartItems == null) {
			return total;
		}

		Set<SimProduct> keySet = cartItems.keySet();

		for (SimProduct sp : keySet) {
			total += cartItems.get(sp) * sp.getPrice();
		}

		for (PriceModifier pm : priceModifiers) {
			total += pm.computeAdjustment(cartItems);
		}
		
		if (appliedPromo != null) {
			total = total - (total * appliedPromo.getDiscount());
		}

		return total;
	}

	public Map<SimProduct, Integer> getAllItems(Map<SimProduct, Integer> cartItems) {
		Map<SimProduct, Integer> totalCartItems = new HashMap<>();
		Map<SimProduct, Integer> totalCartAdjustment = new HashMap<>();

		if (cartItems == null) {
			return totalCartItems;
		}

		// Set<SimProduct> keySet = cartItems.keySet();

		// for (SimProduct sp : keySet) {
		// 	Integer ultCount = cartItems.get(sp);
		// 	if (sp.getCode().equals("ult_medium")) {
		// 		totalCartItems.put(MainApplication.spGb, ultCount);
		// 	}
		// 	totalCartItems.put(sp, ultCount);
		// }

		totalCartItems.putAll(cartItems);

		for (ItemModifier im : itemModifiers) {
			Map<SimProduct, Integer> adjustment = im.computeAdjustment(cartItems);
			
			for (SimProduct sp : adjustment.keySet()) {
				if (totalCartAdjustment.get(sp) != null) {
					totalCartAdjustment.put(sp, totalCartAdjustment.get(sp) + adjustment.get(sp));
				} else {
					totalCartAdjustment.put(sp, adjustment.get(sp));
				}
			}
		}

		for (SimProduct sp : totalCartAdjustment.keySet()) {
			if (totalCartItems.get(sp) != null) {
				totalCartItems.put(sp, totalCartItems.get(sp) + totalCartAdjustment.get(sp));
			} else {
				totalCartItems.put(sp, totalCartAdjustment.get(sp));
			}
		}


		return totalCartItems;
	}
}